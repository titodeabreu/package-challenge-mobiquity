package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.service.PackerService;
import com.mobiquityinc.util.FileUtil;
import com.mobiquityinc.util.ItemUtil;
import com.mobiquityinc.util.PackageUtil;

import java.util.ArrayList;
import java.util.List;

public class Packer {

  private static FileUtil fileUtil = new FileUtil();
  private static PackageUtil packageUtil = new PackageUtil();
  private static ItemUtil itemUtil = new ItemUtil();
  private static PackerService packerService = new PackerService();

  private Packer() {
  }

  public static String pack(String filePath) throws APIException {
    List<String> setRawPackages = fileUtil.readLinesFromFile(filePath);
    List<Package> mountedPackages = retrievePackagesFrom(setRawPackages);
    return retrievePackagesResults(mountedPackages);
  }

  protected static List<Package> retrievePackagesFrom(List<String> setRawPackages) throws APIException {
    if(setRawPackages == null || setRawPackages.isEmpty()) {
      throw new APIException("No set of items available to be packaged.");
    }

    List<Package> packages = new ArrayList<>(0);
    Package packageBox = null;
    List<Item> items = null;
    for(String rawSet: setRawPackages) {
      items = new ArrayList<>(0);
      packageBox = new Package(packageUtil.retrievePackageMaxWeight(rawSet));
      items.addAll(itemUtil.transformRawSetToItems(rawSet));
      packerService.organizePackage(packageBox, items);
      packages.add(packageBox);
    }
    return packages;
  }


  private static String retrievePackagesResults(List<Package> mountedPackages) {
    StringBuilder builder = new StringBuilder();
    for (Package mountedPackage: mountedPackages) {
      String indexItems = mountedPackage.toStringItemsIndex();
      builder.append(indexItems.isEmpty() ? "-" : indexItems).append("\n");
    }
    return builder.toString();
  }
}
