package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemUtil {

    private static final String COLON = ":";

    public List<Item> transformRawSetToItems(String rawSet) throws APIException {
        List<Item> items = new ArrayList<>(0);
        Item item = null;
        String[] itemParameters = null;
        List<String> rawItems = retrieveItemsArrayFrom(rawSet);

        for (String rawItem : rawItems) {
            itemParameters = removeParenthesesAndSplitByComma(rawItem);
            validateItemsParameters(itemParameters, rawItem);
            item = createItemBy(itemParameters);
            items.add(item);
        }

        return items;
    }

    protected List<String> retrieveItemsArrayFrom(String rawSet) throws APIException {
        validateRawSet(rawSet);
        String onlyItems = rawSet.substring(rawSet.indexOf(COLON)+1, rawSet.length()).trim();
        List<String> items = RegexUtil.retrieveOnlyDataInsideParenthesesFrom(onlyItems);

        if(items == null || items.isEmpty()) {
            throw new APIException(String.format("No items present on this set: %s", rawSet));
        }

        return items;
    }

    protected void validateRawSet(String rawSet) throws APIException {

        if(rawSet == null || rawSet.isEmpty()) {
            throw new APIException("Invalid Set of Items is unavailable");
        }

        if(!rawSet.contains(COLON)) {
            throw new APIException(String.format("Invalid pattern for Set of Items. Please validate: %s", rawSet));
        }

    }

    protected String[] removeParenthesesAndSplitByComma(String rawItem) {
        rawItem = rawItem.replace("(", "").replace(")","");
        return rawItem.split(",");
    }

    protected void validateItemsParameters(String[] itemParameters, String rawItem) throws APIException {
        if(itemParameters == null || itemParameters.length != 3) {
            throw new APIException(String.format("No items parameters is invalid for raw item: %s", rawItem));
        }
    }

    protected Item createItemBy(String[] itemParameters) throws APIException {
        return Item.Builder
                .newInstance()
                .addIndex(itemParameters[0])
                .addWeight(itemParameters[1])
                .addCost(itemParameters[2])
                .build();
    }


}
