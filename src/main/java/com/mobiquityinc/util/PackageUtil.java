package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;

public class PackageUtil {

    public static final String COLON = ":";

    public BigDecimal retrievePackageMaxWeight(String rawSet) throws APIException {

            if(rawSet == null || rawSet.isEmpty()) {
                throw new APIException("Invalid Set of Items is unavailable");
            }
            if(!rawSet.contains(COLON)) {
                throw new APIException(String.format("Invalid pattern for Set of Items. Please validate: %s", rawSet));
            }

            String maxWeightString = rawSet.substring(0, rawSet.indexOf(COLON)).trim();

            if(!NumberUtils.isParsable(maxWeightString)) {
                throw new APIException(String.format("Invalid Set of Items has a invalid value for Max Weight. Please validate: %s", rawSet));
            }

            return new BigDecimal(maxWeightString);
    }

}