package com.mobiquityinc.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

    public static List<String> retrieveOnlyDataInsideParenthesesFrom(String value) {
        List<String> data = new ArrayList<>(0);
        Pattern parenthesesRegex = Pattern.compile("\\(.*?\\)");
        Matcher matcher = parenthesesRegex.matcher(value);
        while (matcher.find()) {
            data.add(matcher.group().trim());
        }
        return data;
    }

    public static String removeCurrencyType(String value) {
       return value.replaceAll("\\p{Sc}+", "");
    }

    private RegexUtil(){
    }

}
