package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtil {


    public List<String> readLinesFromFile(String filePath) throws APIException {
        Path path = Paths.get(filePath);
        try (Stream<String> lines = Files.lines(path)) {
            return lines
                    .filter(disregardEmptyLine())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new APIException(String.format("File not able to be loaded. Please validate path: %s", filePath),e);
        }
    }

    private Predicate<String> disregardEmptyLine() {
        return line -> !line.trim().isEmpty();
    }
}
