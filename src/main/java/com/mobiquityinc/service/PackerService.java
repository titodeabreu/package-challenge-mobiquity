package com.mobiquityinc.service;

import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PackerService {

    public void organizePackage(Package packageBox, List<Item> items) {
        sortItemsWithSamePriceToLowerPriority(items);
        sortItemsInHigherPricesAndLowerWeight(items);
        for (Item item : items) {
            packageBox.addItem(item);
        }
    }

    private void sortItemsWithSamePriceToLowerPriority(List<Item> items) {
        Map<BigDecimal, Integer> itemWithSamePriceMap = new HashMap<>(0);
        for(Item item : items) {
            if(itemWithSamePriceMap.get(item.getCost()) == null) {
                itemWithSamePriceMap.put(item.getCost(), 1);
            }else {
                itemWithSamePriceMap.put(item.getCost(), itemWithSamePriceMap.get(item.getCost()) + 1);
            }
        }
        List<BigDecimal> lowPriorityItems = itemWithSamePriceMap
                .keySet()
                .stream()
                .filter(index -> itemWithSamePriceMap.get(index) > 1)
                .collect(Collectors.toList());

        updateItemsWithLowPriority(items, lowPriorityItems);
    }

    public void sortItemsInHigherPricesAndLowerWeight(List<Item> items) {
        Comparator<Item> sortByWeight = (i1, i2) -> i1.getWeight().compareTo(i2.getWeight());
        Comparator<Item> sortByCost = (i1, i2) -> i2.getCost().compareTo(i1.getCost());
        Comparator<Item> sortByLowPriority = (i1, i2) -> Boolean.compare(i1.isLowPriority(),i2.isLowPriority());
        items.sort(sortByCost
                .thenComparing(sortByWeight)
                .thenComparing(sortByLowPriority));
    }

    private void updateItemsWithLowPriority(List<Item> items, List<BigDecimal> lowPriorityItems) {
        for(Item item : items) {
            if(lowPriorityItems.contains(item.getCost())) {
                item.setLowPriority(true);
            }
        }
    }
}
