package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.util.RegexUtil;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.Objects;

public class Item {

    private Long index;
    private BigDecimal weight;
    private BigDecimal cost;
    private boolean lowPriority;

    public Item(Builder builder)
    {
        this.index = builder.index;
        this.weight = builder.weight;
        this.cost = builder.cost;
    }

    public Long getIndex() {
        return index;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setLowPriority(boolean lowPriority) {
        this.lowPriority = lowPriority;
    }

    public boolean isLowPriority() {
        return lowPriority;
    }

    public static class Builder {

        private Long index;
        private BigDecimal weight;
        private BigDecimal cost;

        public static Builder newInstance()
        {
            return new Builder();
        }

        private Builder() {}

        public Builder addIndex(String index) throws APIException {
          
            if(!NumberUtils.isParsable(index)) {
                throw new APIException(String.format("Invalid index: %s", index));
            }
            
            this.index = Long.valueOf(index);
            return this;
        }
        public Builder addWeight(String weight) throws APIException {
            if(!NumberUtils.isParsable(weight)) {
                throw new APIException(String.format("Invalid weight: %s", weight));
            }
            
            this.weight = new BigDecimal(weight);
            return this;
        }
        public Builder addCost(String cost) throws APIException {

            if(!cost.isEmpty()) {
                cost = RegexUtil.removeCurrencyType(cost);

            }

            if(!NumberUtils.isParsable(cost)) {
                throw new APIException(String.format("Invalid cost: %s", cost));
            }
            this.cost = new BigDecimal(cost);
            return this;
        }

        public Item build(){
            return new Item(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(index, item.index) &&
                Objects.equals(weight, item.weight) &&
                Objects.equals(cost, item.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index, weight, cost);
    }

    @Override
    public String toString() {
        return "Item{" +
                "index=" + index +
                ", weight=" + weight +
                ", cost=" + cost +
                '}';
    }
}
