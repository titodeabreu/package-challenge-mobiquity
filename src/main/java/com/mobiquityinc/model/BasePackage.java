package com.mobiquityinc.model;

import java.math.BigDecimal;

public abstract class BasePackage {

    public static final BigDecimal  MAX_ACCEPTABLE_WEIGHT = new BigDecimal(100);
    public static final BigDecimal MAX_ACCEPTABLE_COST = new BigDecimal(100);
    public static final int MAX_ACCEPTABLE_ITEMS_QUANTITY = 15;

    public abstract void addItem(Item item);

}
