package com.mobiquityinc.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Package extends BasePackage {

    List<Item> items = new ArrayList<>(0);
    private BigDecimal maxPackageWeight;

    public Package(BigDecimal maxPackageWeight) {
        this.maxPackageWeight = maxPackageWeight;
    }

    @Override
    public void addItem(Item item) {
        if (isFitable(item) && isPayable(item))
            this.items.add(item);
    }

    private boolean isPayable(Item item) {
        BigDecimal actualCostInPackage = getActualWeightInPackage();
        actualCostInPackage = actualCostInPackage.add(item.getCost());
        return actualCostInPackage.compareTo(MAX_ACCEPTABLE_COST) <= 0;
    }

    private boolean isFitable(Item item) {
        BigDecimal actualWeightInPackage = getActualWeightInPackage();
        actualWeightInPackage = actualWeightInPackage.add(item.getWeight());
        return actualWeightInPackage.compareTo(maxPackageWeight) <= -1
                && actualWeightInPackage.compareTo(MAX_ACCEPTABLE_WEIGHT) <= -1
                && items.size() < MAX_ACCEPTABLE_ITEMS_QUANTITY;
    }

    public BigDecimal getActualWeightInPackage() {
        BigDecimal total = new BigDecimal(0);
        for (Item item : items) {
            total = total.add(item.getWeight());
        }
        return total;
    }

    public BigDecimal getActualCostOfThePackage() {
        BigDecimal total = new BigDecimal(0);
        for (Item item : items) {
            total = total.add(item.getCost());
        }
        return total;
    }

    public String toStringItemsIndex() {
        return this.items.stream().map(item -> item.getIndex().toString()).collect(Collectors.joining(","));
    }

    public List<Item> getItems() {
        return items;
    }

    public BigDecimal getMaxPackageWeight() {
        return maxPackageWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return Objects.equals(maxPackageWeight, aPackage.maxPackageWeight) &&
                Objects.equals(items, aPackage.items);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxPackageWeight, items);
    }

    @Override
    public String toString() {
        return "Package{" +
                "maxPackageWeight=" + maxPackageWeight +
                ", items=" + items +
                '}';
    }
}
