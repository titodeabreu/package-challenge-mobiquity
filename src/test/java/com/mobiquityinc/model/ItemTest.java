package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ItemTest {

    @Test
    public void shouldCreateItemByBuilder() throws APIException {
        //GIVEN
        Long index = 10l;
        BigDecimal weight = BigDecimal.valueOf(10);
        BigDecimal cost = BigDecimal.valueOf(22.598);
        //WHEN
        Item item = Item.Builder
                        .newInstance()
                        .addIndex("10")
                        .addWeight("10")
                        .addCost("$22.¥598€")
                        .build();
        //THEN
        Assert.assertEquals(index,item.getIndex());
        Assert.assertEquals(weight,item.getWeight());
        Assert.assertEquals(cost,item.getCost());
    }

    @Test(expected = APIException.class)
    public void shouldNotCreateItemByBuilderWhenIndexIsInvalid() throws APIException {
        //WHEN
        Item.Builder
                .newInstance()
                .addIndex("V")
                .addWeight("10")
                .addCost("$22.¥59€")
                .build();
    }

    @Test(expected = APIException.class)
    public void shouldNotCreateItemByBuilderWhenWeightIsInvalid() throws APIException {
        //WHEN
        Item.Builder
                .newInstance()
                .addIndex("12")
                .addWeight("1T")
                .addCost("$22.¥59€")
                .build();
    }

    @Test(expected = APIException.class)
    public void shouldNotCreateItemByBuilderWhenCostIsNull() throws APIException {
        //WHEN
        Item.Builder
                .newInstance()
                .addIndex("12")
                .addWeight("1T")
                .addCost(null)
                .build();
    }

}
