package com.mobiquityinc.model;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.service.PackerService;
import com.mobiquityinc.util.FileUtil;
import com.mobiquityinc.util.ItemUtil;
import com.mobiquityinc.util.PackageUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PackageTest {
    private FileUtil fileUtil;
    private ItemUtil itemUtil;
    private PackerService packerService;
    private PackageUtil packageUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
        itemUtil = new ItemUtil();
        packageUtil = new PackageUtil();
        packerService = new PackerService();
    }

    @Test
    public void shouldNotAddItemIfDoNotFit() throws APIException {
        //GIVEN
        String filePath = "src/test/resources/samples/Sample1.txt";
        List<String> setRawPackages = fileUtil.readLinesFromFile(filePath);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(1)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(1));
        //WHEN
        packageBox.addItem(items.get(0));
        //THEN
        Assert.assertEquals(0, packageBox.getItems().size());
    }

    @Test
    public void shouldNotAddItemIfDoNotIsPayable() throws APIException {
        //GIVEN
        String filePath = "src/test/resources/samples/Sample3.txt";
        List<String> setRawPackages = fileUtil.readLinesFromFile(filePath);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(0)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(0));
        packerService.sortItemsInHigherPricesAndLowerWeight(items);
        //WHEN
        packageBox.addItem(items.get(0));
        //THEN
        Assert.assertEquals(0, packageBox.getItems().size());
    }

}
