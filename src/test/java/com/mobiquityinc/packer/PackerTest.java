package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.util.FileUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PackerTest {

    public static final String SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1 = "src/test/resources/samples/Sample1.txt";
    private FileUtil fileUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
    }

    @Test
    public void shouldPackSample1() throws APIException {
        //WHEN
        String packagesResult = Packer.pack(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        //THEN
        assertNotNull(packagesResult);
    }

    @Test
    public void shouldPackSample4ValidateLowPriority() throws APIException {
        //WHEN
        String packagesResult = Packer.pack("src/test/resources/samples/Sample4.txt");
        //THEN
        assertNotNull(packagesResult);
    }

    @Test
    public void shouldPackSample5WithMaxQuantity() throws APIException {
        //GIVEN
        List<String> setItems = fileUtil.readLinesFromFile("src/test/resources/samples/Sample5.txt");
        //WHEN
        List<Package> packagesResult = Packer.retrievePackagesFrom(setItems);
        //THEN
        assertEquals(15,packagesResult.get(0).getItems().size());
    }

    @Test
    public void shouldRetrievePackagesFromSample1() throws APIException {
        List<String> setRawItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        //WHEN
        List<Package> packages = Packer.retrievePackagesFrom(setRawItems);
        //THEN
        assertEquals(4, packages.size());
    }

    @Test
    public void shouldRetrieveItemFromSample1FirstSet() throws APIException {
        List<String> setRawItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        //WHEN
        List<Package> packages = Packer.retrievePackagesFrom(setRawItems);
        //THEN
        assertEquals(1, packages.get(0).getItems().size());
    }

}
