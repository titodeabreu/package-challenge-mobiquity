package com.mobiquityinc.service;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import com.mobiquityinc.model.Package;
import com.mobiquityinc.util.FileUtil;
import com.mobiquityinc.util.ItemUtil;
import com.mobiquityinc.util.PackageUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class PackerServiceTest {
    public static final String SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1 = "src/test/resources/samples/Sample1.txt";
    private FileUtil fileUtil;
    private ItemUtil itemUtil;
    private PackerService packerService;
    private PackageUtil packageUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
        itemUtil = new ItemUtil();
        packageUtil = new PackageUtil();
        packerService = new PackerService();
    }

    @Test
    public void shouldOrganizePackageBoxWithItemsSample1FirstSet() throws APIException {
        //GIVEN
        List<String> setRawPackages = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(0)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(0));
        //WHEN
        packerService.organizePackage(packageBox,items);
        //THEN
        assertEquals("4", packageBox.toStringItemsIndex());
    }

    @Test
    public void shouldOrganizePackageBoxWithItemsSample1SecondSet() throws APIException {
        //GIVEN
        List<String> setRawPackages = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(1)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(1));
        //WHEN
        packerService.organizePackage(packageBox,items);
        //THEN
        assertEquals("", packageBox.toStringItemsIndex());
    }

    @Test
    public void shouldOrganizePackageBoxWithItemsSample1ThirdSet() throws APIException {
        //GIVEN
        List<String> setRawPackages = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(2)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(2));
        //WHEN
        packerService.organizePackage(packageBox,items);
        //THEN
        assertEquals("2,7", packageBox.toStringItemsIndex());
    }

    @Test
    public void shouldOrganizePackageBoxWithItemsSample1FourthSet() throws APIException {
        //GIVEN
        List<String> setRawPackages = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        Package packageBox = new Package(packageUtil.retrievePackageMaxWeight(setRawPackages.get(3)));
        List<Item> items = itemUtil.transformRawSetToItems(setRawPackages.get(3));
        //WHEN
        packerService.organizePackage(packageBox,items);
        //THEN
        assertEquals("8,9", packageBox.toStringItemsIndex());
    }

}
