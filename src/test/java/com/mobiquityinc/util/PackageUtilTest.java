package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class PackageUtilTest {

    public static final String SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1 = "src/test/resources/samples/Sample1.txt";
    private FileUtil fileUtil;
    private PackageUtil packageUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
        packageUtil = new PackageUtil();
    }

    @Test
    public void shouldRetrievePackageMaxWeightFromSample1FirstSet() throws APIException {
        //GIVEN
        List<String> setItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        BigDecimal expectedValue = new BigDecimal(81);
        //WHEN
        BigDecimal maxWeight = packageUtil.retrievePackageMaxWeight(setItems.get(0));
        //THEN
        assertEquals(expectedValue, maxWeight);
    }

    @Test
    public void shouldRetrievePackageMaxWeightFromSample1SecondSet() throws APIException {
        //GIVEN
        List<String> setItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        BigDecimal expectedValue = new BigDecimal(8);
        //WHEN
        BigDecimal maxWeight = packageUtil.retrievePackageMaxWeight(setItems.get(1));
        //THEN
        assertEquals(expectedValue, maxWeight);
    }

    @Test
    public void shouldRetrievePackageMaxWeightFromSample1ThirdSet() throws APIException {
        //GIVEN
        List<String> setItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        BigDecimal expectedValue = new BigDecimal(75);
        //WHEN
        BigDecimal maxWeight = packageUtil.retrievePackageMaxWeight(setItems.get(2));
        //THEN
        assertEquals(expectedValue, maxWeight);
    }

    @Test
    public void shouldRetrievePackageMaxWeightFromSample1FourthSet() throws APIException {
        //GIVEN
        List<String> setItems = fileUtil.readLinesFromFile(SRC_TEST_RESOURCES_SAMPLES_SAMPLE_1);
        BigDecimal expectedValue = new BigDecimal(56);
        //WHEN
        BigDecimal maxWeight = packageUtil.retrievePackageMaxWeight(setItems.get(3));
        //THEN
        assertEquals(expectedValue, maxWeight);
    }

    @Test(expected = APIException.class)
    public void shouldFailedRetrievePackageMaxWeightWithInvalidWeight() throws APIException {
        //GIVEN
        String setItem = "2342w:343";
        //WHEN
        packageUtil.retrievePackageMaxWeight(setItem);
    }

    @Test(expected = APIException.class)
    public void shouldFailedRetrievePackageMaxWeightWithEmptyWeight() throws APIException {
        //GIVEN
        String setItem = ":343";
        //WHEN
        packageUtil.retrievePackageMaxWeight(setItem);
    }

    @Test(expected = APIException.class)
    public void shouldFailedRetrievePackageMaxWeightWithWrongPatternWeight() throws APIException {
        //GIVEN
        String setItem = "123;343";
        //WHEN
        packageUtil.retrievePackageMaxWeight(setItem);
    }

}
