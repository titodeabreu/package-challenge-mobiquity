package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileUtilTest {

    private FileUtil fileUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
    }

    @Test
    public void shouldLoadFileFromAbsolutePath() throws APIException {
        //GIVEN
        String path = "src/test/resources/samples/Sample1.txt";
        //WHEN
        List<String> lines = fileUtil.readLinesFromFile(path);
        //THEN
        assertEquals(4, lines.size());
    }

    @Test
    public void shouldLoadEmptyFileFromAbsolutePath() throws APIException {
        //GIVEN
        String path = "src/test/resources/samples/Sample0";
        //WHEN
        List<String> lines = fileUtil.readLinesFromFile(path);
        //THEN
        assertEquals(0, lines.size());
    }

}
