package com.mobiquityinc.util;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.model.Item;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class ItemUtilTest {

    private FileUtil fileUtil;
    private ItemUtil itemUtil;

    @Before
    public void setUp(){
        fileUtil = new FileUtil();
        itemUtil = new ItemUtil();
    }

    @Test
    public void shouldTransformRawItemsToItemsFromSample1() throws APIException {
        //GIVEN
        String path = "src/test/resources/samples/Sample1.txt";
        List<String> setRawItems = fileUtil.readLinesFromFile(path);
        //WHEN
        List<Item> items = itemUtil.transformRawSetToItems(setRawItems.get(0));
        //THEN
        assertEquals(6, items.size());
    }

    @Test
    public void shouldTransformRawItemsToItemsFromSample2() throws APIException {
        //GIVEN
        String path = "src/test/resources/samples/Sample2.txt";
        List<String> setRawItems = fileUtil.readLinesFromFile(path);
        //WHEN
        List<Item> items = itemUtil.transformRawSetToItems(setRawItems.get(0));
        //THEN
        assertEquals(9, items.size());
    }

    @Test
    public void shouldCreateItemByStringArray() throws APIException {
        //GIVEN
        String[] itemsParams = {"1","1","1"};
        //WHEN
        Item item = itemUtil.createItemBy(itemsParams);
        //THEN
        assertEquals(Long.valueOf(1), item.getIndex());
        assertEquals(BigDecimal.valueOf(1), item.getWeight());
        assertEquals(BigDecimal.valueOf(1), item.getCost());
    }

    @Test(expected = APIException.class)
    public void shouldValidateItemsParamsStructureWithNullParams() throws APIException {
        //WHEN
        itemUtil.validateItemsParameters(null, "");
    }

    @Test(expected = APIException.class)
    public void shouldValidateItemsParamsStructureWithWrongSizeForItem() throws APIException {
        //GIVEN
        String[] itemsParams = {"1","2","4","3","2","1"};
        //WHEN
        itemUtil.validateItemsParameters(itemsParams, "");
    }

    @Test
    public void shouldRemoveParenthesesAndSplitByComma() {
        //GIVEN
        String value = "(((((S0,0)))))2,)))2())()(";
        String[] expected = {"S0","02","2"};
        //WHEN
        String[] results = itemUtil.removeParenthesesAndSplitByComma(value);
        //THEN
        assertArrayEquals(expected, results);
    }

    @Test(expected = APIException.class)
    public void shouldValidateRawSetWhenIsNull() throws APIException {
        //WHEN
        itemUtil.validateRawSet(null);
    }

    @Test(expected = APIException.class)
    public void shouldValidateRawSetWithoutPattern() throws APIException {
        //GIVEN
        String value = "(((((S0,0)))))2,)))2())()(";
        //WHEN
        itemUtil.validateRawSet(value);
    }



}
