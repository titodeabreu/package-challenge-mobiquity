package com.mobiquityinc.util;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RegexUtilTest {

    @Test
    public void shouldRetrieveDataFromInsideParentheses(){
        //GIVEN
        List<String> valueExpected = Arrays.asList("(a)","(b)","(d)");
        String value = "(a)s2325(b)saw2z---(d)  ";
        //WHEN
        List<String> result = RegexUtil.retrieveOnlyDataInsideParenthesesFrom(value);
        //THEN
        assertEquals(valueExpected,result);
    }

    @Test
    public void shouldRetrieveNoDataFromInsideParentheses(){
        //GIVEN
        List<String> valueExpected = new ArrayList<>(0);
        String value = "8293uuiwejdhk23u23e89";
        //WHEN
        List<String> result = RegexUtil.retrieveOnlyDataInsideParenthesesFrom(value);
        //THEN
        assertEquals(valueExpected,result);
    }

}
