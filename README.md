# Package Challenge - Mobiquity

### Package 
- Name: implementation
- Version: 1.0.0

### Build
 mvn clean install
 
For more information's please read Packaging Challenge.pdf

-- Developer Notes

 - I tried to developer all the challenge with TDD, first figure out 
 witch algorithm could be done to fixes the problem.
 - Then i separate into packages to organize the project and each package have
 his owns class. 
 - Create a builder for Item Object with some validations to help construct the object 
 and secure the are no other ways to set the values. 
 - High Coverage and also tried to create meaningful scenarios for the test cases.
 - Organize directory for tests.
 - Use BasePackage as an abstract class with addItem so we can extends for any kind of Package and have the same behavior.
 - Create a Service to handle the logical business for organize the boxes itself.
 - And Package object handle the logic to handle if the item will be added or not.
 - Also create a few Utils and use apache commons library to validate is the String is numeric, only to not replicate code.
 - Add the dependencies in pom, increase the version to 1.0.0 and set the packaging type to jar.
 - Tried a little bit Java 8, looking forward to work with Java 9 or higher.
 
 Thanks.
 Tito Neto.